HoneyGuide-YTB - A Discord Bot
Made this for private usage within our server.

Version: 0.0.1 - POC

Purpose
My friends and I hang out regularly in a discord server. Have been for a long time and if there is one function that is missing from pre-made bots that you can install is the ability to play yt music into voice chat. While we general just do this for a meme reaction, the limitations are annoying so I built this project.

At this time the bot is very basic, but does exactly what I wanted to do. I want to expand the functionality in a couple of different ways but we will see what I think.

Installation
Please see the requirements.txt for dependancies.
Install ffmpeg -> https://ffmpeg.org/

To start:
In terminal
>>> python startup.py



Features:
1. Plays YT videos in the voice channel you are currently active in.
    - Has a basic queue system.
2. Joins Channel
3. Leaves Channel

Planned Features:
1. Delete commands once received and parsed.
2. Display X amount of the queue on request.
3. Add LLM style chat capabilities
4. Anything else I find useful in a discord bot




Name Origin:
I love bees, they are fascinating creature and what is especially interesting it our interaction with them. The Honeyguide is a type of bird found in Africa and Asia. The bird is interesting for the fact it guides people directly to beehives, letting us do the hard work of destroying the nest so they can get the grubs and beeswax left behind. Frankly the name isn't a good match to what is happening, but nonetheless, we give it URLS and it gives us audio streams. Close enough.

The wikipedia page for those interested -> https://en.wikipedia.org/wiki/Honeyguide
