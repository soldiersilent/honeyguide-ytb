from discord.ext import commands
import discord
import yt_dlp
import asyncio
import os
import logging


#Logging Config
logging.basicConfig(level=logging.INFO)


# class for handling the extraction and streaming of YouTube audio
class YTDLSource(discord.PCMVolumeTransformer):
    # configure the options for youtube-dl
    ytdl_format_options = {
        'yes-playlist': True,
        'format': 'bestaudio/best',
        'outtmpl': 'downloads/%(extractor)s-%(id)s-%(title)s.%(ext)s',
        'restrictfilenames': True,
        'nocheckcertificate': True,
        'ignoreerrors': False,
        'logtostderr': False,
        'quiet': True,
        'no_warnings': True,
        'default_search': 'auto',
        'source_address': '0.0.0.0'
    }

    # configure the options for ffmpeg
    ffmpeg_options = {
        'options': '-vn',
        'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5'
    }

    # create an instance of the youtube-dl class with the specified options
    ytdl = yt_dlp.YoutubeDL(ytdl_format_options)

    # the class initializer takes the source audio and related data
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    # this class method takes a URL and returns an instance of the class ready to be played
    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: cls.ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # Handle multiple entries if it's a playlist
            # You can modify this part to add all the entries to the queue or handle them differently
            players = []
            for entry in data['entries']:
                filename = entry['url'] if stream else cls.ytdl.prepare_filename(entry)
                players.append(cls(discord.FFmpegPCMAudio(filename, **cls.ffmpeg_options), data=entry))
            return players
        else:
            filename = data['url'] if stream else cls.ytdl.prepare_filename(data)
            return cls(discord.FFmpegPCMAudio(filename, **cls.ffmpeg_options), data=data)


    def stop(self):
        if self.process.is_alive():
            self.process.terminate()  # forcefully end the process

# class for handling the queue of songs to be played
class SongQueue:
    def __init__(self, bot):
        self.bot = bot
        self.song_urls = []

    async def add_song(self, url):
        logging.info(f"Adding song from URL: {url}")
        player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
        self.song_urls.append(player)
        logging.info(f"Song added to queue: {url}")
        return player

    async def play_next_song(self, ctx):
        voice_client = ctx.guild.voice_client
        if not voice_client or not voice_client.is_connected():  # Check if the bot is not connected
            await ctx.invoke(self.bot.get_command("join"))  # Reconnect the bot if necessary
        if len(self.song_urls) != 0:
            player = self.song_urls.pop(0)  # remove the next song from the queue
            logging.info(f"Playing next song from queue: {player.title}")
            if voice_client.is_playing():
                voice_client.stop()
            voice_client.play(player, after=lambda e: asyncio.run_coroutine_threadsafe(self.play_next_song(ctx), self.bot.loop))
            return player
        else:
            logging.info("No songs in queue, nothing to play")
            return None

    def clear_queue(self):
        self.song_urls = []  # Clear the queue

# set up the bot and its intents
intents = discord.Intents.default()
intents.messages = True
intents.guilds = True
intents.message_content = True

# create an instance of the bot and add the song queue to it
bot = commands.Bot(command_prefix='$', intents=intents)
bot.song_queue = SongQueue(bot)

# define the bot's events
@bot.event
async def on_ready():
    logging.info(f'We have logged in as {bot.user}')


@bot.event
async def on_message(message):
    if message.author == bot.user:  # Ignore messages from the bot itself
        return

    if message.content == 'celeste please shutdown':  # Listen for a 'shutdown' command
        if message.guild.voice_client is not None:
            if message.guild.voice_client.is_playing():
                message.guild.voice_client.stop()
                bot.song_queue.clear_queue()
            await message.guild.voice_client.disconnect()

        await bot.close()  # Stop the bot

    await bot.process_commands(message)  # Process other commands



# bot commands

@bot.command()
async def join(ctx):
    logging.info("Join command invoked")
    if ctx.author.voice is None:
        await ctx.send("You're not in a voice channel!")
    else:
        try:
            channel = ctx.author.voice.channel
            await channel.connect()
        except Exception as e:
            logging.error(f"Error connecting to voice channel: {e}")
            await ctx.send(f"Error connecting to voice channel: {e}")
    logging.info("Join command completed")


@bot.command()
async def leave(ctx):
    try:
        if ctx.voice_client.is_connected():
            await ctx.voice_client.disconnect()
        else:
            await ctx.send("The bot is not connected to a voice channel.")
    except Exception as e:
        logging.error(f"Error leaving voice channel: {e}")
        await ctx.send(f"Error leaving voice channel: {e}")


@bot.command()
async def play(ctx, *, url):
    try:
        if not ctx.voice_client:
            await ctx.invoke(bot.get_command("join"))

        async with ctx.typing():
            player = await ctx.bot.song_queue.add_song(url)

        if player:
            if len(bot.song_queue.song_urls) == 0:
                await ctx.send(f'Now playing: {player.title}')
            elif len(bot.song_queue.song_urls) > 0:
                await ctx.send(f'This song is position {len(bot.song_queue.song_urls)} in the queue')
            if not ctx.voice_client.is_playing():
                await ctx.bot.song_queue.play_next_song(ctx)
    except Exception as e:
        logging.error(f"Error playing the song: {e}")
        await ctx.send(f"Error playing the song: {e}")


@bot.command()
async def stop(ctx):
    try:
        if ctx.voice_client is not None:
            if ctx.voice_client.is_playing():
                ctx.bot.song_queue.current_player.stop()
                ctx.voice_client.stop()
                bot.song_queue.clear_queue()
                await ctx.send("The audio stream and queue have been cleared.")
            else:
                await ctx.send("No audio stream is currently playing.")
        else:
            await ctx.send("The bot is not connected to a voice channel.")
    except Exception as e:
        logging.error(f"Error stopping the audio stream: {e}")
        await ctx.send(f"Error stopping the audio stream: {e}")


@bot.command()
async def skip(ctx):
    try:
        player = ctx.bot.song_queue

        if ctx.voice_client is not None:
            if ctx.voice_client.is_playing():
                ctx.voice_client.stop()
                await asyncio.sleep(1)
                await ctx.send('Skipping current song.')
                await player.play_next_song(ctx)  # this will remove the song from the queue and start the next song
            else:
                await ctx.send("No audio stream is currently playing.")
        else:
            await ctx.send("The bot is not connected to a voice channel.")
    except Exception as e:
        logging.error(f"Error skipping the song: {e}")
        await ctx.send(f"Error skipping the song: {e}")

# get the bot's token from the environment and run the bot
token = os.getenv('DISCORD_BOT_TOKEN')
if token is None:
    logging.info("Your Discord Bot token is not loaded in your environment.")
else:
    bot.run(token)
